package com.jjr.crosswordapp_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrosswordappApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrosswordappApiApplication.class, args);
	}

}
